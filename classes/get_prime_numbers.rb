require_relative '../classes/prime_number_generator'

class GetPrimeNumbers
  def initialize

  end

  def get_primes(range_start, range_end)
    @prime_generator = PrimeNumberGenerator.new()
    prime_numbers = @prime_generator.prime_numbers(range_start, range_end)
    if prime_numbers.empty?
      console_out('This range contains no prime numbers')
      return
    end
    console_out(prime_numbers)
  end

  def console_out(output)
    print output
  end
end