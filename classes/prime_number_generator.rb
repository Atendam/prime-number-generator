class PrimeNumberGenerator
  def initialize
  end

  def prime?(number)
    return false if number < 2
    (2...number).each do |divisible|
      if number % divisible == 0
        return false
      end
    end
    return true
  end

  def prime_numbers(a, b)
    primes = []
    if a > b
     primes = self.prime_numbers(b, a)
    else
      (a...b).each do |number|
        if prime?(number)
          primes << number
        end
      end
    end
    primes
  end

end