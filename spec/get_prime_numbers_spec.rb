require 'rspec'
require_relative '../classes/get_prime_numbers'

describe 'Get prime numbers and return to console' do
  before :each do
    @prime_number = GetPrimeNumbers.new
  end

  describe 'output to console validation' do
    it 'should output to console what i am send it' do
      sent_string = "This is a test"
      expect{ @prime_number.console_out(sent_string) }.to output("This is a test").to_stdout
    end

    it 'should output an array' do
      sent_array = [2,3,5,7]
      expect{ @prime_number.console_out(sent_array) }.to output("[2, 3, 5, 7]").to_stdout
    end
  end

  describe 'Return an array of prime numbers' do

    it 'should return an array of prime numbers to console' do
      expected_array = "[2, 3, 5, 7]"
      expect{  @prime_number.get_primes(1, 10) }.to output(expected_array).to_stdout
    end

  end

  describe 'return a message when range has no prime numbers' do
    it 'should return a string stating there are no prime numbers' do
    expected_string = "This range contains no prime numbers"
    expect{ @prime_number.get_primes(14, 16) }.to output(expected_string).to_stdout
    end

    it 'should not return an empty array to the console' do
      returned_string = @prime_number.get_primes(14, 16)
      expect(returned_string.is_a?(Array)).to be false
    end

  end

end