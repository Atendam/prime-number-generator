require 'rspec'
require_relative '../classes/prime_number_generator'
describe 'prime number generator' do
before :each do
  @prime_generator = PrimeNumberGenerator.new
end
  describe 'Return Prime Numbers' do

    it 'return the prime numbers from range 1-10' do
      prime_number_array = [2, 3, 5, 7]
      generated_prime_numbers = @prime_generator.prime_numbers(1,10)
      expect(generated_prime_numbers).to eql(prime_number_array)
    end

    it 'return the prime numbers from range 10-1' do
      prime_number_array = [2, 3, 5, 7]
      generated_prime_numbers = @prime_generator.prime_numbers(10,1)
      expect(generated_prime_numbers).to eql(prime_number_array)
    end

    it 'return the prime numbers from range 7900 - 7920' do
      prime_number_array = [7901, 7907, 7919]
      generated_prime_numbers = @prime_generator.prime_numbers(7900,7920)
      expect(generated_prime_numbers).to eql(prime_number_array)
    end

    it 'return the prime numbers from range 7920 - 7900' do
      prime_number_array = [7901, 7907, 7919]
      generated_prime_numbers = @prime_generator.prime_numbers(7920,7900)
      expect(generated_prime_numbers).to eql(prime_number_array)
    end

    it 'return an empty array if no primes in range' do
      prime_number_array = []
      generated_prime_numbers = @prime_generator.prime_numbers(14, 16)
      expect(generated_prime_numbers).to eql(prime_number_array)
    end

  end

  describe 'Tests for prime?' do

    it 'return false if number is less than 2' do
      expect(@prime_generator.prime?(1)).to be false
    end

    it 'return true if number is prime for first 26 prime numbers' do
      list_of_primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,
                        101]
      list_of_primes.each do |x|
        expect(@prime_generator.prime?(x)).to be true
      end
    end

    it 'return false if number is not prime' do
      list_of_primes = [4, 6, 8, 9, 10, 12, 14, 15, 26, 18, 20, 21, 22, 24, 25, 26, 27, 28, 60, 81, 100]
      list_of_primes.each do |x|
        expect(@prime_generator.prime?(x)).to be false
      end
    end

  end
end