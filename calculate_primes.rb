require_relative 'classes/get_prime_numbers'

class CalculatePrimes
  def self.main
    @prime = GetPrimeNumbers.new
    @prime.console_out('Please input range start: ')
    range_start = $stdin.gets
    range_start = range_start.chomp
    @prime.console_out('Please input range end: ')
    range_end = $stdin.gets
    range_end = range_end.chomp
    @prime.get_primes(range_start.to_i, range_end.to_i)
  end
  CalculatePrimes.main
end