# Prime Number Generator
This project takes in two numbers and will return the prime numbers between them. 

## Installation
- install ruby
- install devkit
- install bundler `gem install bundler`
- install require gems `bundle install`

## To run
- open command prompt or terminal
- `ruby calculate_primes.rb`
- follow prompts 

Enjoy